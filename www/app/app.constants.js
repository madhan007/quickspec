var CONSTANTS={
	appName:'QuickSpec',
	modules:{
		services:'QuickSpec.services',
		controllers:'QuickSpec.controllers',
		directives:'QuickSpec.directives',
		utils:'QuickSpec.utils'
	},
	routes:{
		sideMenu:{
			url:'/appHome',
			templateUrl:'app/components/side-menu/side-menu.html',
			controller:'SideMenuController'
		},
		home:{
			url:'/home',
			templateUrl:'app/components/home/home.html'
		},
		products:{
			url:'/products/',
			templateUrl:'app/components/products/products.html',
			controller:'ProductsController'
		},
		reviews:{
			url:'/reviews/',
			templateUrl:'app/components/reviews/reviews.html',
			controller:'ReviewsController'
		}
	},
	serviceMethods:{
		getReviews:'getReviews',
		getProducts:'getProducts'
	},
	reqMethods:{
		get:'GET',
		post:'POST'
	},
	reqConfigs:{
		baseUrl:'http://quickspec.herokuapp.com/'
	}

}