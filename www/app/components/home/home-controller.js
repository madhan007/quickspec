controllers.controller('HomeController',['$scope','$log','$utilFactory','$state',function($scope,$log,$utilFactory,$state){
	 $scope.categories=[{pathId:'pcmcat209400050001',categoryName:'Mobiles',class:'fa fa-tablet'},{ pathId:'abcat0100000',categoryName:'TVs & Home Theatre', class:'fa fa-television'},{ pathId:'abcat0404000',categoryName:'Memory Cards', class:'ion-ios-film-outline'},
     { pathId:'abcat0201010',categoryName:'Mp3 Players', class:'fa fa-music'},{ pathId:'pcmcat331200050001',categoryName:'Headphones', class:'fa fa-headphones'},{ pathId:'pcmcat310200050004',categoryName:'Speakers', class:'fa fa-volume-up'}
     ,{ pathId:'pcmcat332000050000',categoryName:'Wearables', class:'ion-android-watch'},{ pathId:'abcat0401005',categoryName:'Cameras', class:'fa fa-camera'}]

     $scope.categoryClick=function(pathId){
        $state.go('products',{'pathId':pathId});
     }
}])