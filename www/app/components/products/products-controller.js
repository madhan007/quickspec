controllers.controller('ProductsController',['$scope','$log','$utilFactory','$state','$stateParams',function($scope,$log,$utilFactory,$state,$stateParams){
		$scope.$on('$ionicView.beforeEnter', function() {
				$utilFactory.showLoader();
        		$log.debug('ProductsController-beforeEnter');
        		console.log($stateParams.pathId);
        		var reqData={};
				reqData['searchTerm']="*";
				reqData['categoryPathId']=$stateParams.pathId;
           var promise=$utilFactory.sendRequest(CONSTANTS.serviceMethods.getProducts,CONSTANTS.reqMethods.get,reqData);	
	         promise.then(function(response){
				$log.log("controller",response);		
				$utilFactory.hideLoader();
				$scope.products=response.data.products;
			   },function(error){
					$log.log("controller",error);
					$utilFactory.hideLoader();
					$utilFactory.displayAlert('Request Timed Out!','Please try again later!');
             });
       });

	   $scope.getReviews=function(product){
	   	   var productString=JSON.stringify(product);
	   	   $state.go('reviews',{'data':productString});
	   }
}])