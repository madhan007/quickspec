Foodie
.controller('SignInCtrl',function($scope,$ionicPopup,$state,LoginService,AuthService)
{

   $scope.data={}
   $scope.signIn = function() {
     AuthService.login($scope.data.username, $scope.data.password).then(function(data) {
            $state.go('app.notifications.invitations');
             $scope.setCurrentUsername(data.username);
        },function(data) {
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Please check your credentials!'
            });
        });
  };
  $scope.signUp=function(){
    console.log("signup");
    $state.go('signup');
  };
})