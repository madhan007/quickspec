controllers.controller('ReviewsController',['$scope','$log','$utilFactory','$state','$stateParams',function($scope,$log,$utilFactory,$state,$stateParams){
		$scope.results={};
		$scope.results['reviewResults']={};
		$scope.results.reviewResults['noOfPositive']=0;
		$scope.results.reviewResults['noOfNegative']=0
		$scope.results.reviewResults['totalScore']=0;
		$scope.rating=[];
		$scope.clickClass="";
		 $scope.showReviews=false;
		n=4;
		$scope.$on('$ionicView.beforeEnter', function() {
				$utilFactory.showLoader();
        		$log.debug('ReviewsController-beforeEnter');
        		console.log($stateParams.data);
        		var data=JSON.parse($stateParams.data);
        		console.log('stateParams',data);
        		var reqData={};
				reqData['sku']=data.sku;
				$scope.data=data;
           var promise=$utilFactory.sendRequest(CONSTANTS.serviceMethods.getReviews,CONSTANTS.reqMethods.get,reqData);	
	         promise.then(function(response){
				$log.log("ReviewsController ",response);		
				$utilFactory.hideLoader();
				$scope.results=response.data;
				if(response.data.reviewResults!=undefined && response.data.reviewResults.avgRating!=undefined && !isNaN(response.data.reviewResults.avgRating))
					n=parseInt(response.data.reviewResults.avgRating);
				for(i=0;i<n;i++){
					$scope.rating.push(i);
				}
			   },function(error){
					$log.log("ReviewsController",error);
					$utilFactory.hideLoader();
					$utilFactory.displayAlert('Request Timed Out!','Please try again later!');
             });
       });

		$scope.readReviews=function(){
			 $scope.showReviews=true;

		}
		$scope.openReview=function(){
			$scope.clickClass="wrap";
		}
}])