utils.factory('$utilFactory',['$q','$http','$rootScope','$log','$ionicPopup','$ionicLoading','$ionicNavBarDelegate',function($q,$http,$rootScope,$log,$ionicPopup,$ionicLoading,$ionicNavBarDelegate) {
	return{
		sendRequest:function(serviceMethod,reqHttpMethod,data){
			$log.debug("sendRequest Util");
			 var deferred = $q.defer();
			 var config={};
			   config['timeout']=10000;
			if(reqHttpMethod==CONSTANTS.reqMethods.get){
				deferred = $q.defer();
				config['params']=data;
				$http.get(CONSTANTS.reqConfigs.baseUrl+serviceMethod,config).then(function(response){
					$log.log("Response",response);
					deferred.resolve(response);
				},function(error){
					$log.error("error",error);
					deferred.reject(error);
				});
				return deferred.promise;
			}
			else if(reqHttpMethod==CONSTANTS.reqMethods.post){
				$http.post(CONSTANTS.reqConfigs.baseUrl+serviceMethod,data,config).then(function(response){
					$log.log("Response",response);
					deferred.resolve(response);
				},function(error){
					$log.error("error",error);
					deferred.reject(error);
				});
			}
			return deferred.promise;
		},

		displayAlert:function(title,message){
			$ionicPopup.alert({
		     title: title,
		     template: message
		   });
		},
		getFromJson:function(url){
		var deferred=$q.defer();
		$log.debug("getFromJson Service");
		$http.get(url).then(function(response){
				$log.log("response",response);
				deferred.resolve(response);
		},
		function(error){
			    $log.log(error);
			    deferred.reject(error);
		});
		return deferred.promise;
	   },
	   showLoader:function(){
	   		$ionicLoading.show({
                            template: '<ion-spinner class="ios-small"></ion-spinner>'
                       });
	   },
	   hideLoader: function() {
                        $ionicLoading.hide();
                   },
	   localStorage:{
	   	setItem:function(key,value){
	   		window.localStorage.setItem(key,value);
	   	},
	   	getItem:function(key){
	   		return window.localStorage.getItem(key);
	   	},
	   	removeItem:function(key){
	   		window.localStorage.removeItem(key);
	   	}
	   },
	   setNavBarTitle:function(title){
		    $ionicNavBarDelegate.title(title);
	   }



	}
}])