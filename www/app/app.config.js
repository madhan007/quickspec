app.config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('appHome',{
		url: CONSTANTS.routes.sideMenu.url,
		templateUrl:CONSTANTS.routes.sideMenu.templateUrl,
		controller:CONSTANTS.routes.sideMenu.controller
	})
	.state('home',{
	   url:CONSTANTS.routes.home.url,
        views:{
          'menuView':{
              templateUrl:CONSTANTS.routes.home.templateUrl,
              controller:CONSTANTS.routes.home.controller
          }
        }
	})
	.state('products',{
	   url:CONSTANTS.routes.products.url+":pathId",
        views:{
          'menuView':{
              templateUrl:CONSTANTS.routes.products.templateUrl,
              controller:CONSTANTS.routes.products.controller
          }
        },
       
	})
	.state('reviews',{
	   url:CONSTANTS.routes.reviews.url+":data",
        views:{
          'menuView':{
              templateUrl:CONSTANTS.routes.reviews.templateUrl,
              controller:CONSTANTS.routes.reviews.controller
          }
        },
       
	})
	$urlRouterProvider.otherwise(CONSTANTS.routes.home.url)
}])