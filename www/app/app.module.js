var services = angular.module(CONSTANTS.modules.services,[]);
var controllers = angular.module(CONSTANTS.modules.controllers,[]);
var utils=angular.module(CONSTANTS.modules.utils,[]);
app=angular.module(CONSTANTS.appName, ['ionic',CONSTANTS.modules.services,CONSTANTS.modules.controllers,CONSTANTS.modules.utils]);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
